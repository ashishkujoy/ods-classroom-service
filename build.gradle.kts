plugins {
    kotlin("jvm") version "1.3.72"
    id("application")
    id("com.adarshr.test-logger") version "2.0.0"
    id("java")
    id("org.jetbrains.kotlin.kapt") version "1.3.31"
    `maven-publish`
}

group = "io.ods.classroom"
version = "1.0.0"

repositories {
    mavenCentral()
}

dependencies {
    val kotestVersion = "4.0.5"
    val micronautVersion = "1.3.3"
    implementation(kotlin("stdlib-jdk8"))
    annotationProcessor(platform("io.micronaut:micronaut-bom:$micronautVersion"))
    api("io.micronaut:micronaut-runtime:$micronautVersion")
    api("io.micronaut:micronaut-http-client:$micronautVersion")
    annotationProcessor(platform("io.micronaut:micronaut-bom:$micronautVersion"))
    kapt("io.micronaut:micronaut-inject-java:$micronautVersion")
    kaptTest("io.micronaut:micronaut-inject-java:$micronautVersion")
    testAnnotationProcessor("io.micronaut:micronaut-inject-java:$micronautVersion")
    testImplementation("io.micronaut:micronaut-inject-java:$micronautVersion")
    testImplementation(platform("io.micronaut:micronaut-bom:$micronautVersion"))
    testImplementation("io.micronaut:micronaut-inject:$micronautVersion")
    testImplementation("io.micronaut:micronaut-validation:$micronautVersion")
    testImplementation("io.micronaut:micronaut-http-server-netty:$micronautVersion")
    testImplementation("io.micronaut.test:micronaut-test-kotest:1.2.0.RC1")
    testImplementation("io.kotest:kotest-runner-junit5-jvm:$kotestVersion")
    testImplementation("io.kotest:kotest-runner-console-jvm:$kotestVersion")
    testImplementation("io.kotest:kotest-assertions-core-jvm:$kotestVersion")
    testImplementation("io.github.ashishkujoy:micronaut-kotest-wrapper:1.0.0")
    testImplementation("io.mockk:mockk:1.9.3")
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}