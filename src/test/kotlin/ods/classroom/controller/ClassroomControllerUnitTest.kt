package ods.classroom.controller

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.micronaut.core.type.Argument
import io.micronaut.http.HttpRequest
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.test.annotation.MicronautTest
import io.micronaut.test.annotation.MockBean
import io.micronaut.test.extensions.kotest.MicronautKotestExtension.getMock
import io.mockk.every
import io.mockk.mockk
import ods.classroom.service.ClassroomService
import javax.inject.Inject

@MicronautTest
class ClassroomControllerUnitTest(
        @Inject @Client("/classroom") val httpClient: RxHttpClient,
        @Inject val classroomService: ClassroomService
) : StringSpec({
    "should get class for a given student id" {
        every { getMock(classroomService).getByStudentId("123") } returns listOf("123")
        val httpResponse = httpClient.toBlocking()
                .exchange(HttpRequest.GET<List<String>>("/student/123"), Argument.listOf(String::class.java))
        httpResponse.body() shouldBe listOf("123")
    }
}) {
    @MockBean(ClassroomService::class)
    fun classroomService() : ClassroomService = mockk(relaxed = true)
}