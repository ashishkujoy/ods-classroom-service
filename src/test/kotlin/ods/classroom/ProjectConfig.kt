package ods.classroom

import io.kotest.core.config.AbstractProjectConfig
import io.kotest.core.extensions.Extension
import io.kotest.core.listeners.Listener
import io.micronaut.test.extensions.kotest.MicronautKotestExtension

object ProjectConfig : AbstractProjectConfig() {
    override fun listeners(): List<Listener> = listOf(MicronautKotestExtension)
    override fun extensions(): List<Extension> = listOf(MicronautKotestExtension)
}