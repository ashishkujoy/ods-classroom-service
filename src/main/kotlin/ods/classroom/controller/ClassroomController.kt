package ods.classroom.controller

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.PathVariable
import ods.classroom.service.ClassroomService

@Controller("/classroom")
class ClassroomController(private val classroomService: ClassroomService) {

    @Get("/student/{id}")
    fun classroomsByStudentId(@PathVariable id: String): List<String> {
        return classroomService.getByStudentId(id)
    }
}